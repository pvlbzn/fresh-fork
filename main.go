/*
TWEAKED. SEE DOCS.
*/
package main

import (
	"os"

	"gitlab.com/pvlbzn/fresh-fork/runner"
)

func main() {
	args := os.Args[1:]
	runner.Start(args)
}
